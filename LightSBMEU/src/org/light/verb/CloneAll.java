package org.light.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.VerbFactory;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.generator.NamedStatementListGenerator;
import org.light.utils.InterVarUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class CloneAll extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() {
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception  {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() {
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("cloneAll"+StringUtil.capFirst(this.domain.getPlural()));
			method.setReturnType(new Type("void"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1,InterVarUtil.DB.ids.getVarName(),InterVarUtil.DB.ids.getVarType()));
			return method;
		}
	}

	@Override
	public String generateServiceMethodDefinitionString() {
		if (this.denied) return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("cloneAll"+StringUtil.capFirst(this.domain.getPlural()));
			method.setReturnType(new Type("void"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken()+"."+this.domain.getDaoSuffix()+"."+this.domain.getStandardName()+"Dao");
			method.addAdditionalImport(this.domain.getPackageToken()+"."+this.domain.getServiceSuffix()+"."+this.domain.getStandardName()+"Service");
			method.addSignature(new Signature(1,"ids",new Type("String")));
			method.addMetaData("Override");
			
			//Service method
			Method servicemethod = new Method();
			servicemethod.setStandardName("clone"+StringUtil.capFirst(this.domain.getStandardName()));
			servicemethod.setReturnType(new Type("void"));
			servicemethod.setThrowException(true);
			servicemethod.addSignature(new Signature(1,this.domain.getDomainId().getLowerFirstFieldName(),this.domain.getDomainId().getRawType()));
							
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(1000L,2,"String [] idArr = ids.split(\",\");"));
			list.add(new Statement(2000L,2,"for (String idString : idArr){"));
			if (this.domain.getDomainId().getRawType().isLong()){
				list.add(new Statement(3000L,3,this.domain.getDomainId().getClassType() +" "+this.domain.getDomainId().getLowerFirstFieldName()+" = Long.valueOf(idString);"));
			} else if (this.domain.getDomainId().getRawType().isInt()){
				list.add(new Statement(3000L,3,this.domain.getDomainId().getClassType() +" "+this.domain.getDomainId().getLowerFirstFieldName()+" = Integer.valueOf(idString);"));
			}	
			list.add(new Statement(4000L,3,servicemethod.getStandardName()+"("+this.domain.getDomainId().getLowerFirstFieldName()+");"));
			list.add(new Statement(5000L,2,"}"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}

	@Override
	public String generateServiceImplMethodString() {
		if (this.denied) return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() {
		if (this.denied) return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}
	
	public CloneAll(){
		super();
		this.setLabel("批克隆");
	}	
	
	public CloneAll(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("CloneAll");
		this.setVerbName("CloneAll"+StringUtil.capFirst(this.domain.getPlural()));
		this.setLabel("批克隆");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("CloneAll");
	}

	@Override
	public Method generateControllerMethod() {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("cloneAll"+StringUtil.capFirst(this.domain.getPlural()));
			method.setReturnType(new Type("Map<String,Object>"));
			method.setThrowException(true);
			method.addAdditionalImport(this.domain.getPackageToken()+"."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken()+"."+this.domain.getServiceSuffix()+"."+this.domain.getStandardName()+"Service");
			method.addSignature(new Signature(1,"ids",new Type("String"), "","RequestParam(value = \"ids\", required = true)"));
			method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"\", method = RequestMethod.POST)");
	
			List<Writeable> wlist = new ArrayList<Writeable>();
			Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
			Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
			Var ids = new Var("ids",new Type("String"));
			wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
			wlist.add(NamedStatementGenerator.getFacadeCallServiceMethodByIds(2000L, 2,this.domain, service, generateServiceMethodDefinition(),ids));
			wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndNull(3000L, 2, resultMap));
			wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName()+";"));	
			method.setMethodStatementList(WriteableUtil.merge(wlist));
			
			return method;
		}
	}

	@Override
	public String generateControllerMethodString() {
		if (this.denied) return null;
		else {
			Method m = this.generateControllerMethod();
			return m.generateMethodString();
		}
	}

	@Override
	public String generateControllerMethodStringWithSerial() {
		if (this.denied) return null;
		else {
			Method m = this.generateControllerMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied) return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("cloneAll"+domain.getCapFirstPlural());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000,0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'CloneAll',"));
			}else {
				sl.add(new Statement(2000,1, "text:'批克隆',"));
			}
			sl.add(new Statement(3000,1, "iconCls:'icon-add',"));
			sl.add(new Statement(4000,1, "handler:function(){ "));
			sl.add(new Statement(5000,2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000,2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000,3, "$.messager.alert(\"警告\",\"请选定记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000,3, "return;"));
			sl.add(new Statement(9000,2, "}"));
			sl.add(new Statement(10000,2, "var ids = \"\";"));
			sl.add(new Statement(11000,2, "for(var i=0;i<rows.length;i++){"));
			sl.add(new Statement(12000,3, "ids += rows[i][\""+domain.getDomainId().getLowerFirstFieldName()+"\"];"));
			sl.add(new Statement(13000,3, "if (i < rows.length-1) ids += \",\";"));
			sl.add(new Statement(14000,2, "}"));
			sl.add(new Statement(15000,2, "cloneAll"+domain.getCapFirstPlural()+"(ids);"));
			sl.add(new Statement(16000,1, "}"));
			sl.add(new Statement(17000,0, "}"));
			block.setMethodStatementList(sl);
			return block;			
		}
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		if (this.denied) return null;
		else {
			return generateEasyUIJSButtonBlock().generateBlockContentString();
		}
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		if (this.denied) return null;
		else {
			return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
		}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied) return null;
		else {
			Domain domain = this.domain;
			Var ids = new Var("ids",new Type("var"));
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("cloneAll"+StringUtil.capFirst(domain.getPlural()));
			Signature s1 = new Signature();
			s1.setName(ids.getVarName());
			s1.setPosition(1);
			s1.setType(new Type("var"));
			method.addSignature(s1);
			
			StatementList sl = new StatementList();
			sl.add(new Statement(1000,1, "$.ajax({"));
			sl.add(new Statement(2000,2, "type: \"post\","));
			sl.add(new Statement(3000,3, "url: \"../"+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/cloneAll"+domain.getCapFirstPlural()+"\","));
			sl.add(new Statement(4000,3, "data: {"));
			sl.add(new Statement(5000,4, "ids:ids"));
			sl.add(new Statement(6000,3, "},"));
			sl.add(new Statement(7000,3, "dataType: 'json',"));
			sl.add(new Statement(8000,3, "success: function(data, textStatus) {"));
			sl.add(new Statement(9000,4, "$(\"#dg\").datagrid(\"load\");"));
			sl.add(new Statement(10000,3, "},"));
			sl.add(new Statement(11000,3, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(12000,2, "},"));
			sl.add(new Statement(13000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(14000,3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(15000,3, "alert(errorThrown.toString());"));
			sl.add(new Statement(16000,2, "}"));
			sl.add(new Statement(17000,1, "});"));
			
			method.setMethodStatementList(sl);
			return method;		
		}
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		if (this.denied) return null;
		else {
			return generateEasyUIJSActionMethod().generateMethodString();
		}
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		if (this.denied) return null;
		else {
			return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
		}
	}
}
