package org.light.generator;

import org.light.domain.Domain;
import org.light.domain.Type;
import org.light.domain.Var;

public class NamedTokenGenerator {
	public static String getStringtoNumberToken(Type type){
		if (type.getTypeName().equals("int")) return "Integer.parseInt(";
		else if (type.getTypeName().equals("long")) return "Long.parseLong(";
		else if (type.getTypeName().equals("double")) return "Double.parseDouble(";
		else if (type.getTypeName().equals("float")) return "Float.parseFloat(";
		else if (type.getTypeName().equals("boolean")) return "Boolean.parseBoolean(";
		else return "";
	}
	
	public static String getDaomainList(Domain domain, Var list){
		String result = "List<"+domain.getStandardName()+"> "+list.getVarName();
		return result;
	}
}
