package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.light.core.Writeable;
import org.light.domain.IndependentConfig;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class ApplicationPropertiesGenerator extends IndependentConfig{
	private static final long serialVersionUID = 3204734849992863577L;
	protected String dbUserName;
	protected String dbUserPassword;
	protected String dbType;
	protected String dbName;
	protected String packageToken;

	public ApplicationPropertiesGenerator(){
		super();
		this.fileName = "application.properties";
		this.standardName = "applicationProperties";
		this.folder = "src/main/resources/";
	}
	
	@Override
	public String generateImplString() {
		List<Writeable> sList = new ArrayList<Writeable>();
		if (this.dbType.equalsIgnoreCase("mysql")||this.dbType.equalsIgnoreCase("mariadb")) {
			sList.add(new Statement(1000L,0,"spring.datasource.url = jdbc:mysql://localhost:3306/"+this.dbName+"?useUnicode=true&characterEncoding=UTF-8"));
			sList.add(new Statement(2000L,0,"spring.datasource.username = "+this.dbUserName));
			sList.add(new Statement(3000L,0,"spring.datasource.password ="+this.dbUserPassword));
			sList.add(new Statement(4000L,0,"spring.datasource.driverClassName = com.mysql.jdbc.Driver"));
			sList.add(new Statement(5000L,0,""));
			sList.add(new Statement(6000L,0,"spring.jackson.serialization.indent-output=true"));
			sList.add(new Statement(7000L,0,""));
			sList.add(new Statement(8000L,0,"#mybatis.config-location=classpath:mybatis-config.xml"));
			sList.add(new Statement(9000L,0,"mybatis.type-aliases-package="+this.packageToken+".domain"));
			sList.add(new Statement(10000L,0,"mybatis.mapper-locations=classpath:mapper/*.xml"));
			sList.add(new Statement(11000L,0,""));
			sList.add(new Statement(12000L,0,"spring.profiles.active=prod"));
		}else if (this.dbType.equalsIgnoreCase("oracle")) {
			sList.add(new Statement(1000L,0,"spring.datasource.url = jdbc:oracle:thin:@localhost:1521:"+this.dbName));
			sList.add(new Statement(2000L,0,"spring.datasource.username = "+this.dbUserName));
			sList.add(new Statement(3000L,0,"spring.datasource.password ="+this.dbUserPassword));
			sList.add(new Statement(4000L,0,"spring.datasource.driverClassName = oracle.jdbc.OracleDriver"));
			sList.add(new Statement(5000L,0,""));
			sList.add(new Statement(6000L,0,"spring.jackson.serialization.indent-output=true"));
			sList.add(new Statement(7000L,0,""));
			sList.add(new Statement(8000L,0,"#mybatis.config-location=classpath:mybatis-config.xml"));
			sList.add(new Statement(9000L,0,"mybatis.type-aliases-package="+this.packageToken+".domain"));
			sList.add(new Statement(10000L,0,"mybatis.mapper-locations=classpath:mapper/*.xml"));
			sList.add(new Statement(11000L,0,""));
			sList.add(new Statement(12000L,0,"spring.profiles.active=prod"));
		}
		return WriteableUtil.merge(sList).getContent();
	}

	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbUserPassword() {
		return dbUserPassword;
	}

	public void setDbUserPassword(String dbUserPassword) {
		this.dbUserPassword = dbUserPassword;
	}

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getPackageToken() {
		return packageToken;
	}

	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
}
