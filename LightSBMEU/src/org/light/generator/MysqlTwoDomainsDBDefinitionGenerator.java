package org.light.generator;

import org.light.domain.Domain;
import org.light.utils.SqlReflector;
import org.light.utils.StringUtil;

public class MysqlTwoDomainsDBDefinitionGenerator{
	protected Domain master;
	protected Domain slave;
	
	public MysqlTwoDomainsDBDefinitionGenerator(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
	}

	public String generateDBSql(String dbtype) throws Exception{
		StringBuilder sb = new StringBuilder();
		
		sb.append(SqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
		return sb.toString();
	}
	
	public String generateDropLinkTableSql() throws Exception{
		if (StringUtil.isBlank(slave.getAlias())){
			String result = "drop table if exists " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getStandardName())) +";\n";
			return result;
		}else {
			String result = "drop table if exists " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getAlias())) +";\n";
			return result;
		}
	}
	
	public String generateOracleDropLinkTableSql() throws Exception{
		if (StringUtil.isBlank(slave.getAlias())){
			String result = "drop table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getStandardName())) +";\n";
			return result;
		}else {
			String result = "drop table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getAlias())) +";\n";
			return result;
		}
	}
}
