package org.light.utils;

import org.light.domain.Domain;

public class TableStringUtil {
	public static String domainNametoTableName(Domain domain) throws Exception {
		return StringUtil.changeDomainFieldtoTableColum(domain.getPlural());
	}
	
	public static String domainNametoCapTableName(Domain domain) throws Exception {
		return (domain.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(domain.getPlural())).toUpperCase();
	}
	
	public static String domainNametoLowerTableName(Domain domain) throws Exception {
		return (domain.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(domain.getPlural())).toLowerCase();
	}
	
	public static String domainNametoTableNameWithDbPrefix(Domain domain) throws Exception {
		return domain.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(domain.getPlural());
	}
	
	public static String twoDomainNametoTableNameWithDbPrefix(Domain master,Domain slave) throws Exception {
		if (StringUtil.isBlank(slave.getAlias())){
			return master.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(master.getCapFirstDomainName())+"_"+StringUtil.changeDomainFieldtoTableColum(slave.getCapFirstDomainName());
		}else{
			return master.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(master.getCapFirstDomainName())+"_"+StringUtil.changeDomainFieldtoTableColum(StringUtil.capFirst(slave.getAlias()));
		}
	}

	public static String domainIdNametoTableFieldName(Domain domain) {
		return StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName()).toLowerCase();
	}
}
