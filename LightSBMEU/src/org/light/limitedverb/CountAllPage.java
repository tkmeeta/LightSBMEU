package org.light.limitedverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.exception.ValidateException;
import org.light.utils.InterVarUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class CountAllPage extends NoControllerVerb {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("countAll" + domain.getCapFirstDomainName() + "Records");
			method.setNoContainer(true);
			List<Writeable> list = new ArrayList<Writeable>();
			Var countNum = new Var("countNum", new Type("Integer"));
			list.add(new Statement(100L, 1,
					"<select id=\"" + method.getLowerFirstMethodName() + "\" resultType=\"Integer\">"));
			list.add(new Statement(200L, 2, MybatisSqlReflector.generateCountRecordStatement(domain, countNum)));
			list.add(new Statement(300L, 1, "</select>"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			String s = m.generateMethodString();
			return s;
		}
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("countAll" + this.domain.getCapFirstDomainName() + "Records");
			method.setReturnType(new Type("Integer"));
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.setThrowException(true);
			return method;
		}
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("countAll" + StringUtil.capFirst(this.domain.getPlural()) + "Page");
			method.setReturnType(new Type("Integer"));
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1, "pagesize", new Type("Integer")));
			method.setThrowException(true);

			return method;
		}
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("countAll" + StringUtil.capFirst(this.domain.getPlural()) + "Page");
			method.setReturnType(new Type("Integer"));
			method.addSignature(new Signature(1, "pagesize", new Type("Integer")));
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDaoSuffix() + "."
					+ this.domain.getStandardName() + "Dao");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");
			method.setThrowException(true);
			method.addMetaData("Override");

			Method daomethod = this.generateDaoMethodDefinition();

			List<Writeable> list = new ArrayList<Writeable>();
			Var pagesize = new Var("pagesize", new Type("Integer"));
			Var pagecount = new Var("pagecount", new Type("Integer"));
			Var recordcount = new Var("recordcount", new Type("Integer"));
			list.add(new Statement(1000L, 2,
					"if (" + pagesize.getVarName() + " <= 0) " + pagesize.getVarName() + " = 10;"));
			list.add(new Statement(2000L, 2, "Integer " + pagecount.getVarName() + " = 1;"));
			list.add(new Statement(3000L, 2, "Integer " + recordcount.getVarName() + " = "
					+ daomethod.generateStandardServiceImplCallString(InterVarUtil.DB.dao.getVarName()) + ";"));
			list.add(new Statement(4000L, 2, pagecount.getVarName() + " = (int)Math.ceil((double)"
					+ recordcount.getVarName() + "/" + pagesize.getVarName() + ");"));
			list.add(new Statement(5000L, 2,
					"if (" + pagecount.getVarName() + " <= 1)" + pagecount.getVarName() + " = 1;"));
			list.add(new Statement(6000L, 2, "return " + pagecount.getVarName() + ";"));
			method.setMethodStatementList(WriteableUtil.merge(list));

			return method;
		}
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	public CountAllPage(Domain domain) throws ValidateException {
		super();
		this.domain = domain;
		this.domain = domain;
		this.denied = true;
		this.verbName = "countAll" + this.domain.getPlural() + "Page";
	}

	public CountAllPage() {
		super();
	}

}
