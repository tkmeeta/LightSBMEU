package org.light.core;

public interface ICloneable extends Cloneable{
	public Object clone();
}
