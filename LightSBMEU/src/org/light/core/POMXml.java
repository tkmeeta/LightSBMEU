package org.light.core;

import java.util.ArrayList;
import java.util.List;

import org.light.domain.IndependentConfig;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class POMXml extends IndependentConfig{	
	private static final long serialVersionUID = 1881392732324115362L;
	protected String projectName;
	protected String dbType = "mysql";
	public POMXml(){
		super();		
		this.standardName = "pomXml";
		this.fileName = "pom.xml";
	}
	
	public POMXml(String dbType){
		this();
		this.dbType = dbType;
	}
	
	@Override
	public String generateImplString() {
    	List<Writeable> sList = new ArrayList<Writeable>();
    	sList.add(new Statement(1000L,0,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
    	sList.add(new Statement(2000L,0,"<project xmlns=\"http://maven.apache.org/POM/4.0.0\""));
    	sList.add(new Statement(3000L,0,"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""));
    	sList.add(new Statement(4000L,0,"xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">"));
    	sList.add(new Statement(5000L,0,"<packaging>war</packaging>"));
    	sList.add(new Statement(6000L,0,"<modelVersion>4.0.0</modelVersion>"));
    	sList.add(new Statement(7000L,0,""));
    	sList.add(new Statement(8000L,0,"<groupId>org.javaforever</groupId>"));
    	sList.add(new Statement(9000L,0,"<artifactId>"+this.projectName+"</artifactId>"));
    	sList.add(new Statement(10000L,0,"<version>1.0-SNAPSHOT</version>"));
    	sList.add(new Statement(11000L,0,""));
    	sList.add(new Statement(12000L,0,"<parent>"));
    	sList.add(new Statement(13000L,0,"<groupId>org.springframework.boot</groupId>"));
    	sList.add(new Statement(14000L,0,"<artifactId>spring-boot-starter-parent</artifactId>"));
    	sList.add(new Statement(15000L,0,"<version>2.0.0.RELEASE</version>"));
    	sList.add(new Statement(16000L,0,"<relativePath />"));
    	sList.add(new Statement(17000L,0,"</parent>"));
    	sList.add(new Statement(18000L,0,""));
    	sList.add(new Statement(19000L,0,"<dependencies>"));
    	sList.add(new Statement(20000L,0,"<dependency>"));
    	sList.add(new Statement(21000L,1,"<groupId>org.apache.commons</groupId>"));
    	sList.add(new Statement(22000L,1,"<artifactId>commons-lang3</artifactId>"));
    	sList.add(new Statement(23000L,0,"<version>3.0</version>"));
    	sList.add(new Statement(24000L,0,"</dependency>"));
    	sList.add(new Statement(25000L,0,"<dependency>"));
    	sList.add(new Statement(26000L,1,"<groupId>commons-io</groupId>"));
    	sList.add(new Statement(27000L,1,"<artifactId>commons-io</artifactId>"));
    	sList.add(new Statement(28000L,1,"<version>2.2</version>"));
    	sList.add(new Statement(29000L,0,"</dependency>"));
    	sList.add(new Statement(30000L,0,"<dependency>"));
    	sList.add(new Statement(31000L,1,"<groupId>org.apache.poi</groupId>"));
    	sList.add(new Statement(32000L,1,"<artifactId>poi</artifactId>"));
    	sList.add(new Statement(33000L,1,"<version>4.0.1</version>"));
    	sList.add(new Statement(34000L,0,"</dependency>"));
    	sList.add(new Statement(35000L,0,"<dependency>"));
    	sList.add(new Statement(36000L,1,"<groupId>org.springframework.boot</groupId>"));
    	sList.add(new Statement(37000L,1,"<artifactId>spring-boot-starter-web</artifactId>"));
    	sList.add(new Statement(38000L,0,"<exclusions>"));
    	sList.add(new Statement(39000L,1,"<exclusion>"));
    	sList.add(new Statement(40000L,2,"<groupId>org.springframework.boot</groupId>"));
    	sList.add(new Statement(41000L,2,"<artifactId>spring-boot-starter-tomcat</artifactId>"));
    	sList.add(new Statement(42000L,0,"</exclusion>"));
    	sList.add(new Statement(43000L,0,"</exclusions>"));
    	sList.add(new Statement(44000L,0,"</dependency>"));
    	sList.add(new Statement(45000L,0,"<dependency>"));
    	sList.add(new Statement(46000L,1,"<groupId>javax.servlet</groupId>"));
    	sList.add(new Statement(47000L,1,"<artifactId>javax.servlet-api</artifactId>"));
    	sList.add(new Statement(48000L,1,"<version>3.1.0</version>"));
    	sList.add(new Statement(49000L,0,"</dependency>"));
    	sList.add(new Statement(50000L,0,"<dependency>"));
    	sList.add(new Statement(51000L,1,"<groupId>org.springframework.boot</groupId>"));
    	sList.add(new Statement(52000L,1,"<artifactId>spring-boot-starter-tomcat</artifactId>"));
    	sList.add(new Statement(53000L,1,"<scope>provided</scope>"));
    	sList.add(new Statement(54000L,0,"</dependency>"));
    	sList.add(new Statement(55000L,0,"<dependency>"));
    	sList.add(new Statement(56000L,0,"<groupId>org.mybatis.spring.boot</groupId>"));
    	sList.add(new Statement(57000L,0,"<artifactId>mybatis-spring-boot-starter</artifactId>"));
    	sList.add(new Statement(58000L,0,"<version>1.2.0</version>"));
    	sList.add(new Statement(59000L,0,"</dependency>"));
    	if ("mysql".equalsIgnoreCase(this.dbType)||"mariadb".equalsIgnoreCase(this.dbType)) {
	    	sList.add(new Statement(60000L,0,"<dependency>"));
	    	sList.add(new Statement(61000L,1,"<groupId>mysql</groupId>"));
	    	sList.add(new Statement(62000L,1,"<artifactId>mysql-connector-java</artifactId> "));
	    	sList.add(new Statement(63000L,0,"</dependency>"));
    	} else if ("oracle".equalsIgnoreCase(this.dbType)){
	    	sList.add(new Statement(60000L,0,"<dependency>"));
	    	sList.add(new Statement(61000L,1,"<groupId>com.oracle.jdbc</groupId>"));
	    	sList.add(new Statement(62000L,1,"<artifactId>ojdbc6</artifactId>"));
	    	sList.add(new Statement(62000L,1,"<version>11.2.0.4</version>"));
	    	sList.add(new Statement(62100L,1,"<scope>system</scope>"));
	    	sList.add(new Statement(62200L,1,"<systemPath>${basedir}/src/main/webapp/WEB-INF/lib/ojdbc6.jar</systemPath>"));
	    	sList.add(new Statement(63000L,0,"</dependency>"));
    	}
    	sList.add(new Statement(64000L,0,"<dependency>"));
    	sList.add(new Statement(65000L,1,"<groupId>com.itextpdf</groupId>"));
    	sList.add(new Statement(66000L,1,"<artifactId>itext-asian</artifactId>"));
    	sList.add(new Statement(67000L,1,"<version>5.2.0</version>"));
    	sList.add(new Statement(68000L,0,"</dependency>"));
    	sList.add(new Statement(69000L,0,"<dependency>"));
    	sList.add(new Statement(70000L,1,"<groupId>com.itextpdf</groupId>"));
    	sList.add(new Statement(71000L,1,"<artifactId>itextpdf</artifactId>"));
    	sList.add(new Statement(72000L,1,"<version>5.4.3</version>"));
    	sList.add(new Statement(73000L,0,"</dependency>"));
		sList.add(new Statement(74000L,0,"</dependencies>"));
    	sList.add(new Statement(75000L,0,"</project>"));
    	return WriteableUtil.merge(sList).getContent();
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

}
