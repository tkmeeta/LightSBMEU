package org.light.domain;

import java.io.Serializable;

public class Pair implements Serializable, Comparable<Pair>{
	private static final long serialVersionUID = -489079226503463242L;
	protected String key;
	protected String value;
	
	public Pair(String key, String value){
		super();
		this.key = key;
		this.value = value;
	}
	@Override
	public int compareTo(Pair pair) {
		return this.key.compareTo(pair.getKey());
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
