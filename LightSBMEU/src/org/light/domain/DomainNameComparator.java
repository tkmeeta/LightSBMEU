package org.light.domain;

import java.io.Serializable;
import java.util.Comparator;

public class DomainNameComparator implements Comparator,Serializable  
{  
    public int compare(Object o1,Object o2)  
    {  
        Domain d1 = (Domain)o1;  
        Domain d2 = (Domain)o2;  
        return d1.getStandardName().compareTo(d2.getStandardName());
    }  
}  
