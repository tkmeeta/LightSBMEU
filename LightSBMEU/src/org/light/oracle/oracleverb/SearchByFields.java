package org.light.oracle.oracleverb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.limitedverb.CountSearchByFieldsRecords;
import org.light.utils.DomainTokenUtil;
import org.light.utils.StringUtil;
import org.light.utils.TableStringUtil;
import org.light.utils.WriteableUtil;

public class SearchByFields extends Verb implements EasyUIPositions {
	protected CountSearchByFieldsRecords countSearch = new CountSearchByFieldsRecords();

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.setNoContainer(true);
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(100L, 1, "<select id=\"" + method.getLowerFirstMethodName() + "\" resultMap=\""
					+ this.domain.getLowerFirstDomainName() + "\">"));
			list.add(new Statement(200L, 2, "select " + DomainTokenUtil.generateTableCommaFields(domain) + " from "
					+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain)));
			list.add(new Statement(300L, 2, "where 1=1 "));
			long serial = 400L;
			Set<Field> fields = this.domain.getFieldsWithoutId();
			for (Field f : fields) {
				if (f.getFieldType().equalsIgnoreCase("string")) {
					list.add(new Statement(serial, 2, "<if test=\"" + f.getLowerFirstFieldName() + "!=null and "
							+ f.getLowerFirstFieldName() + "!='' \">"));
					list.add(new Statement(serial + 100, 3,
							"and " + DomainTokenUtil.changeDomainFieldtoTableColum(f.getLowerFirstFieldName())
									+ " LIKE CONCAT(CONCAT('%', #{" + f.getLowerFirstFieldName() + "}),'%')"));
				} else {
					list.add(new Statement(serial, 2, "<if test=\"" + f.getLowerFirstFieldName() + "!=null\">"));
					list.add(new Statement(serial + 100, 3,
							"and " + DomainTokenUtil.changeDomainFieldtoTableColum(f.getLowerFirstFieldName()) + " = #{"
									+ f.getLowerFirstFieldName() + "}"));
				}
				list.add(new Statement(serial + 200, 2, "</if>"));
				serial += 300L;
			}
			list.add(new Statement(serial, 2,
					"  order by to_number(" + TableStringUtil.domainIdNametoTableFieldName(domain) + ")"));
			list.add(new Statement(serial + 100L, 1, "</select>"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			String s = m.generateMethodString();
			return s;
		}
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.setReturnType(new Type("List", this.domain, this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1, this.domain.getLowerFirstDomainName(), this.domain.getType()));
			method.setThrowException(true);

			return method;
		}
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.setReturnType(new Type("List", this.domain, this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1, this.domain.getLowerFirstDomainName(), this.domain.getType()));
			method.setThrowException(true);

			return method;
		}
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.setReturnType(new Type("List", this.domain, this.domain.getPackageToken()));
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1, this.domain.getLowerFirstDomainName(), this.domain.getType()));
			method.setThrowException(true);

			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDomainSuffix() + "."
					+ this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getDaoSuffix() + "."
					+ this.domain.getStandardName() + "Dao");
			method.addAdditionalImport(this.domain.getPackageToken() + "." + this.domain.getServiceSuffix() + "."
					+ this.domain.getStandardName() + "Service");

			method.addMetaData("Override");

			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(3000L, 2, "return dao.search" + this.domain.getCapFirstPlural() + "ByFields("
					+ this.domain.getLowerFirstDomainName() + ");"));
			method.setMethodStatementList(WriteableUtil.merge(list));

			return method;
		}
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	public SearchByFields() {
		super();
		this.setLabel("按字段搜索");
	}

	public SearchByFields(Domain domain) throws ValidateException {
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("SearchByFields");
		this.setVerbName("search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
		this.setLabel("按字段搜索");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("SearchByFields");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return null;
	}

}
