package org.light.oracle.oracleverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class Export extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}	

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception  {
		return null;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception  {
		return null;
	}
	
	public Export(){
		super();
		this.setLabel("Excel导出");
	}
	
	public Export(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("Export");
		this.setVerbName("Export"+this.domain.getCapFirstPlural());
		this.setLabel("Excel导出");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("ExportExcel");
	}

	@Override
	public Method generateControllerMethod()  throws Exception {	if (this.denied) return null; else {
		Method method = new Method();
		method.setStandardName("export"+this.domain.getCapFirstPlural());
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
		method.addAdditionalImport(this.domain.getPackageToken()+"."+this.domain.getServiceSuffix()+"."+this.domain.getStandardName()+"Service");
		method.addAdditionalImport(this.domain.getPackageToken()+".utils.POIExcelUtil");
		method.addAdditionalImport("java.io.OutputStream");
		method.addAdditionalImport("java.util.Arrays");
		method.addAdditionalImport("java.util.ArrayList");

		for (Field f:this.domain.getFields()) {
			if (f instanceof Dropdown) {
				Dropdown dp = (Dropdown)f;
				method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+dp.getTarget().getCapFirstDomainNameWithSuffix());
				method.addAdditionalImport(this.domain.getPackageToken()+"."+this.domain.getServiceSuffix()+"."+dp.getTarget().getStandardName()+"Service");
			}
		}
		method.addAdditionalImport("javax.servlet.http.HttpServletRequest");
		method.addAdditionalImport("javax.servlet.http.HttpServletResponse");
		method.addAdditionalImport("javax.servlet.http.HttpSession");
		method.addSignature(new Signature(1,"session",new Type("HttpSession")));
		method.addSignature(new Signature(2,"response",new Type("HttpServletResponse")));
		method.addSignature(new Signature(3,"request",new Type("HttpServletRequest")));
		method.addMetaData("RequestMapping(value = \"/export"+this.domain.getCapFirstPlural()+"\")");
		
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,2,"try(OutputStream out = response.getOutputStream()){"));
		sList.add(new Statement(2000L,3,"response.addHeader(\"Content-Disposition\", \"attachment;filename="+this.domain.getCapFirstPlural()+".xls\");"));
		sList.add(new Statement(3000L,3,"List<"+this.domain.getCapFirstDomainNameWithSuffix()+"> list = service.listAll"+this.domain.getCapFirstPlural()+"();"));
		sList.add(new Statement(4000L,3,"List<List<String>> contents = new ArrayList<>();"));
		if  (domain.getLanguage().equalsIgnoreCase("english"))  {
			sList.add(new Statement(5000L,3,"String sheetName = \""+this.domain.getText()+" Information\";"));
		}else {
			sList.add(new Statement(5000L,3,"String sheetName = \""+this.domain.getText()+"信息表\";"));			
		}
		sList.add(new Statement(6000L,3,"String [] headers = "+this.domain.generateFieldLabelsArrayStr(this.domain.getFields())+";"));
		sList.add(new Statement(7000L,3,""));
		sList.add(new Statement(8000L,3,"for ("+this.domain.getCapFirstDomainNameWithSuffix()+" "+this.domain.getLowerFirstDomainName()+":list) {"));
		long serial = 9000L;
		for (Field f:this.domain.getFields()) {
			if (f instanceof Dropdown) {
				Dropdown dp = (Dropdown)f;
				sList.add(new Statement(serial,4,f.getFieldType()+ " "+f.getLowerFirstFieldName()+" = "+this.domain.getLowerFirstDomainName()+".get"+f.getCapFirstFieldName()+"();"));
				sList.add(new Statement(serial+500L,4,dp.getTarget().getType()+" " +dp.getAliasName()+"Type0;"));
				sList.add(new Statement(serial+1000L,4,dp.getTarget().getDomainName().getFieldType()+" " +dp.getAliasName()+"Name0 = \"\";"));
				sList.add(new Statement(serial+2000L,4,"if ("+f.getLowerFirstFieldName()+" != null) {"));
				sList.add(new Statement(serial+2100L,5,dp.getAliasName()+"Type0 = "+StringUtil.lowerFirst(dp.getTargetName())+"Service.find"+dp.getTarget().getCapFirstDomainName()+"By"+dp.getTarget().getDomainId().getCapFirstFieldName()+"("+f.getLowerFirstFieldName()+");"));
				sList.add(new Statement(serial+2200L,5,"if ("+dp.getAliasName()+"Type0 !=null) "+dp.getAliasName()+"Name0 = "+dp.getAliasName()+"Type0.get"+dp.getTarget().getDomainName().getCapFirstFieldName()+"();"));
				sList.add(new Statement(serial+2300L,4,"}"));
				serial+=3000L;
			}
		}
		
		String sc = "String [] row = {";
		for (Field f:this.domain.getFields()) {
			if (f instanceof Dropdown) {
				Dropdown dp = (Dropdown)f;
				sc += dp.getAliasName() + "Name0,";
			}else {
				if (f.getFieldType().equals("String")) {
					sc += this.domain.getLowerFirstDomainName()+".get"+f.getCapFirstFieldName()+"(),";
				}else {
					sc += this.domain.getLowerFirstDomainName()+".get"+f.getCapFirstFieldName()+"().toString(),";
				}
			}
		}
		if (this.domain.getFields()!=null && this.domain.getFields().size()>0) {
			sc = sc.substring(0,sc.length()-1);
		}
		sc += "};";
		sList.add(new Statement(serial,4,sc));
		sList.add(new Statement(serial+1000L,4,"contents.add(Arrays.asList(row));"));
		sList.add(new Statement(serial+2000L,3,"}"));
		sList.add(new Statement(serial+3000L,3,""));
		sList.add(new Statement(serial+4000L,3,"POIExcelUtil.exportExcelSheet(out, sheetName, Arrays.asList(headers), contents);"));
		sList.add(new Statement(serial+5000L,2,"}"));		
		method.setMethodStatementList(WriteableUtil.merge(sList));		
		return method;
	}
	}
	

	@Override
	public String generateControllerMethodString() throws Exception {	if (this.denied) return null; else {
		Method m = this.generateControllerMethod();
		return m.generateMethodString();
	}
	}

	@Override
	public String generateControllerMethodStringWithSerial()  throws Exception {	if (this.denied) return null; else {
		Method m = this.generateControllerMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock()  throws Exception {	if (this.denied) return null; else {
		JavascriptBlock block = new JavascriptBlock();
		block.setSerial(100);
		block.setStandardName("export"+domain.getCapFirstPlural());
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,0, "{"));
		if (this.domain.getLanguage().equalsIgnoreCase("english")) {
			sl.add(new Statement(2000, 1, "text:'Export Excel',"));
		}else {
			sl.add(new Statement(2000,1, "text:'Excel导出',"));
		}
		sl.add(new Statement(3000,1, "iconCls:'icon-sum',"));
		sl.add(new Statement(4000,1, "handler:function(){"));
		sl.add(new Statement(5000,2, "window.location.href=\"../"+this.domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/export"+this.domain.getCapFirstPlural()+"\";"));
		sl.add(new Statement(6000,1, "}"));
		sl.add(new Statement(7000,0, "}"));
		block.setMethodStatementList(sl);
		return block;			
	}
	}

	@Override
	public String generateEasyUIJSButtonBlockString()  throws Exception {	if (this.denied) return null; else {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial()  throws Exception {	if (this.denied) return null; else {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;	
	}

	@Override
	public String generateEasyUIJSActionString()  throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return null;
	}
}
