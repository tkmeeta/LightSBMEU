package org.light.oracle.generator;

import org.light.domain.Domain;
import org.light.generator.DBDefinitionGenerator;
import org.light.utils.SqlReflector;

public class Oracle11gDBDefinitionGenerator extends DBDefinitionGenerator{

	public String generateDBSql(boolean createDB) throws Exception{
		StringBuilder sb = new StringBuilder();
			
		for (int i=0; i < this.getDomains().size();i++){
			Domain domain = this.getDomains().get(i);
			sb.append(Oracle11gSqlReflector.generateTableDefinition(domain)).append("\n");
		}
		return sb.toString();
	}
	
	public String generateDropTableSqls(boolean createDB) throws Exception{
		StringBuilder sb = new StringBuilder();
		for (int i=0; i < this.getDomains().size();i++){
			Domain domain = this.getDomains().get(i);
			sb.append(Oracle11gSqlReflector.generateDropTableStatement(domain)).append("\n");
		}
		sb.append("\n");
		return sb.toString();
	}
	
	public String generateDropTableSqls() throws Exception{
		StringBuilder sb = new StringBuilder();
			
		for (int i=0; i < this.getDomains().size();i++){
			Domain domain = this.getDomains().get(i);
			sb.append(SqlReflector.generateDropTableStatement(domain)).append("\n");
		}
		return sb.toString();
	}
	
	public String generateDBSql() throws Exception{
		return generateDBSql(false);
	}
	
}
