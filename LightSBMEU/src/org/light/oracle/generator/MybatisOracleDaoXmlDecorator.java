package org.light.oracle.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.DomainNameComparator;
import org.light.domain.Field;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.WriteableUtil;

public class MybatisOracleDaoXmlDecorator {
	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Set<Domain> getResultMaps() {
		return resultMaps;
	}

	public void setResultMaps(Set<Domain> resultMaps) {
		this.resultMaps.addAll(resultMaps);
	}

	public Set<Method> getDaoXmlMethods() {
		return daoXmlMethods;
	}

	public void setDaoXmlMethods(Set<Method> daoXmlMethods) {
		this.daoXmlMethods.addAll(daoXmlMethods);
	}
	public void addDaoXmlMethod(Method method) {
		if (method != null){
			this.daoXmlMethods.add(method);
		}
	}
	public void addResultMap(Domain domain) {
		this.resultMaps.add(domain);
	}
	protected Domain domain;
	protected Set<Domain> resultMaps = new TreeSet<Domain>(new DomainNameComparator());
	protected Set<Method> daoXmlMethods = new TreeSet<Method>();

	public StatementList generateDaoXmlResultMaps(long serial,int indent,Domain domain){
		StatementList result = new StatementList();
		result.add(new Statement(serial,indent,"<resultMap id=\""+domain.getLowerFirstDomainName()+"\" type=\""+domain.getFullName()+"\">"));
		result.add(new Statement(serial+100L,indent+1,"<id column=\""+domain.getDomainId().getTableColumnName()+"\" property=\""+domain.getDomainId().getLowerFirstFieldName()+"\"/>"));
		int fieldSize = domain.getFieldsWithoutId().size();
		Field[] fields = domain.getFieldsWithoutId().toArray(new Field[fieldSize]);		
		for (int i=0;i<fieldSize;i++){
			Field f = fields[i];
			result.add(new Statement(serial+200L+100L*i,indent+1,"<result column=\""+f.getTableColumnName()+"\" property=\""+f.getLowerFirstFieldName()+"\"/>"));		
		}
		result.add(new Statement(serial+300L+100L*fieldSize,indent,"</resultMap>"));
		result.add(new Statement(serial+300L+100L*fieldSize+100L,indent,""));
		result.setSerial(serial);
		result.setIndent(indent);
		return result;
	}
	
	public String generateMybatisDaoXmlFileStr(){
		List<Writeable> result = new ArrayList<Writeable>();
		result.add(new Statement(100L,0,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
		result.add(new Statement(200L,0,"<!DOCTYPE mapper PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\" \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">"));
		result.add(new Statement(300L,0,"<mapper namespace=\""+this.domain.getPackageToken()+"."+this.domain.getDaoSuffix()+"."+this.domain.getStandardName()+"Dao"+"\"> "));
		
		int resultMapsize = this.resultMaps.size();
		int i = 0;
		for (Domain d: this.resultMaps){
			result.add(generateDaoXmlResultMaps(400L+100L*i,1,d));
			i++;
		}
		
		int daoXmlMehtodSize = this.daoXmlMethods.size();
		i = 0;
		for (Method m:this.daoXmlMethods){
			if (m!= null) {
				m.setNoContainer(true);
				m.setSerial(400L+100L*resultMapsize+100L*i);
				result.add(m.getMethodStatementList());
				result.add(new Statement(400L+100L*resultMapsize+100L*i+50L,0,""));
			}			
			i++;
		}
		
		result.add(new Statement(400L+100L*(resultMapsize+daoXmlMehtodSize),0,"</mapper>"));
		return WriteableUtil.merge(result).getContent();
	}
}
