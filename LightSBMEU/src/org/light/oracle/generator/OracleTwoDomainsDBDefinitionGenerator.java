package org.light.oracle.generator;

import org.light.domain.Domain;

public class OracleTwoDomainsDBDefinitionGenerator{
	protected Domain master;
	protected Domain slave;
	
	public OracleTwoDomainsDBDefinitionGenerator(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
	}

	public String generateDBSql() throws Exception{
		StringBuilder sb = new StringBuilder();

		sb.append(Oracle11gSqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
		return sb.toString();
	}

}
